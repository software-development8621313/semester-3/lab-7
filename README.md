# Spring Security

A segurança é fundamental para proteger os dados e garantir a integridade de um sistema. O Spring Security desempenha um papel crucial ao fornecer uma estrutura robusta e personalizável para autenticação e autorização em aplicações web. Ele simplifica a implementação de práticas de segurança, como controle de acesso e proteção contra ataques, permitindo aos desenvolvedores focarem no desenvolvimento de funcionalidades sem se preocuparem com questões de segurança. Dessa forma, o Spring Security ajuda a garantir a confidencialidade e a privacidade dos usuários, fortalecendo a segurança da aplicação como um todo.

## Objetivo

Criar um sistema web simples para gerenciamento de usuários e permissões usando Spring Security. O sistema deve permitir que um usuário faça login usando suas credenciais (nome de usuário e senha) e, com base em suas permissões, acesse diferentes áreas do sistema.

## Testes

### Registro de usuário

O usuário se registra com o username, senha a sua role. O retorno para o caso de sucesso é 200.

![Register image](assets/register.jpeg)

Após se registrar, ele pode logar com sua conta. O retorno é um token de acesso que ele usará para as outras requests, caso contrário, ele será impedido pelo sistema.

![Login image](assets/login.jpeg)

O Access token é passado nos headers como Bearer. A ferramenta que estou usando para essas requests abstrai isso.

![Access token](assets/access-token.jpeg)

Caso o token for válido, ele pode acessar os recursos

![Get products success image](assets/get-products-sucess.jpeg)

Caso contrário, ele será barrado.

![Get products error image](assets/get-products-error.jpeg)

Em alguns casos, mesmo passando o access token, ele só poderá acessar algumas rotas se ele for Admin. Neste caso, só o usuário com a role ADMIN pode inserir novos produtos no sistema.

![Roles image](assets/roles.jpeg)

Caso o usuário seja validado, o retorno será de sucesso.

![Admin request success](assets/register-new-admin.jpeg)
![Admin request success](assets/login-as-admin.jpeg)
![Admin request success](assets/authorization-success.jpeg)

Caso contrário, ele será barrado.

![Admin request error](assets/authorization-error.jpeg)

## Requisitos funcionais

- [x] O Admin deve ser capaz de adicionar novos produtos
- [x] O Admin deve ser capaz de remover produtos 

[//]: # (- [ ] Um Customer deve ser capaz de adicionar novos items ao carrinho)
[//]: # (- [ ] Um Customer deve ser capaz de realizar uma compra)

## Regras de negócio

- [x] Um Usuário deve ser capaz de se registrar no sistema
- [x] Um Usuário deve ser capaz de logar na sua conta
- [x] Apenas usuários Admin podem adicionar novos produtos 

[//]: # (- [ ] Um Customer só pode adicionar um item com estoque suficiente)

- [x] O login deve retornar um access token
- [x] Um usuário deve passar o access token em todas requisições do sistema, salvo as de autenticação e ver todos os produtos

## End points

### Autenticação

```http request
POST /api/auth/register HTTP/2
Host: localhost:8080
Content-Type: application/json

{
  "username": "user",
  "password": "123456789",
  "role": "CUSTOMER"
}
```

```http request
POST /api/auth/login HTTP/2
Host: localhost:8080
Content-Type: application/json

{
  "username": "user",
  "password": "123456789"
}
```

### Produtos

```http request
GET /api/products HTTP/2
Host: localhost:8080
Content-Type: application/json
Authorization: Bearer "token"
```

```http request
POST /api/products HTTP/2
Host: localhost:8080
Content-Type: application/json
Authorization: Bearer "token"

{
  "lable": "produto",
  "price": "123.45",
  "stock": 34
}
```
