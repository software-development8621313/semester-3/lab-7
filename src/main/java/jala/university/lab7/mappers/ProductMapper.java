package jala.university.lab7.mappers;

import jala.university.lab7.domain.entities.Product;
import jala.university.lab7.http.dtos.request.ProductRequestDTO;
import jala.university.lab7.http.dtos.response.ProductResponseDTO;
import org.springframework.beans.BeanUtils;

public class ProductMapper {
    public static Product toEntity(ProductRequestDTO dto) {
        return new Product(dto.label(), dto.price(), dto.stock());
    }

    public static ProductResponseDTO toResponseDTO(Product product) {
        return new ProductResponseDTO(product.getId(), product.getLabel(), product.getPrice(), product.getStock(), product.getCreatedAt());
    }
}
