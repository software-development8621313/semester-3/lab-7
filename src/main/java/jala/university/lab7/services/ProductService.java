package jala.university.lab7.services;

import jala.university.lab7.domain.entities.Product;
import jala.university.lab7.domain.repositories.ProductRepository;
import jala.university.lab7.services.exceptions.ProductNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;

    public List<Product> findAll() {
        return productRepository.findAll();
    }

    public Product findById(String id) {
        if(productExists(id)) {
            throw new ProductNotFoundException();
        }

        return productRepository.getReferenceById(id);
    }

    public Product create(Product product) {
        return productRepository.save(product);
    }

    public void delete(String id) {
        if(!productExists(id)) {
            throw new ProductNotFoundException();
        }

        productRepository.deleteById(id);
    }

    private boolean productExists(String id) {
        return productRepository.findById(id).isPresent();
    }
}
