package jala.university.lab7.http.controllers;

import jala.university.lab7.domain.entities.Product;
import jala.university.lab7.http.dtos.request.ProductRequestDTO;
import jala.university.lab7.http.dtos.response.ProductResponseDTO;
import jala.university.lab7.mappers.ProductMapper;
import jala.university.lab7.services.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/products")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;

    @GetMapping
    public ResponseEntity<List<ProductResponseDTO>> getAll() {
        var products = productService.findAll();
        var response = new ArrayList<ProductResponseDTO>();

        for (Product product : products) {
            var productResponseDTO = ProductMapper.toResponseDTO(product);
            response.add(productResponseDTO);
        }

        return ResponseEntity.ok(response);
    }

    @PostMapping
    public ResponseEntity<ProductResponseDTO> create(@RequestBody ProductRequestDTO body) {
        var newProduct = productService.create(ProductMapper.toEntity(body));
        var response = ProductMapper.toResponseDTO(newProduct);

        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductResponseDTO> getById(@PathVariable String id) {
        var product = productService.findById(id);
        var response = ProductMapper.toResponseDTO(product);

        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable String id) {
        productService.delete(id);

        return ResponseEntity.ok().build();
    }
}
