package jala.university.lab7.http.controllers;

import jala.university.lab7.domain.entities.User;
import jala.university.lab7.domain.repositories.UserRepository;
import jala.university.lab7.http.dtos.request.AuthRequestDTO;
import jala.university.lab7.http.dtos.request.RegisterRequestDTO;
import jala.university.lab7.http.dtos.response.AuthResponseDTO;
import jala.university.lab7.services.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthController {
    private final AuthenticationManager authenticationManager;
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<Object> auth(@RequestBody AuthRequestDTO body) {
        return authService.login(body);
    }

    @PostMapping("/register")
    public ResponseEntity<Object> register(@RequestBody RegisterRequestDTO body) {
        return authService.register(body);
    }
}
