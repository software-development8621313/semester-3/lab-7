package jala.university.lab7.http.dtos.request;

import jala.university.lab7.domain.entities.UserRole;
import org.hibernate.validator.constraints.Length;

public record RegisterRequestDTO(@Length(min = 3) String username, @Length(min = 3) String password, UserRole role) {
}
