package jala.university.lab7.http.dtos.response;

import java.math.BigDecimal;
import java.time.Instant;

public record ProductResponseDTO(String id, String label, BigDecimal price, int stock, Instant createdAt) {
}
