package jala.university.lab7.domain.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import java.math.BigDecimal;
import java.time.Instant;

@Entity
@Table(name = "TB_PRODUCT")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "product_id")
    private String id;

    @Column(nullable = false)
    private String label;

    @Column(name = "VL_PRICE", nullable = false)
    private BigDecimal price;

    @Column(name = "NR_QUANTITY", nullable = false)
    private int stock;

    @CreationTimestamp
    @Column(name = "DT_CREATED_AT")
    private Instant createdAt;

    public Product(String label, BigDecimal price, int stock) {
        this.label = label;
        this.price = price;
        this.stock = stock;
    }
}
